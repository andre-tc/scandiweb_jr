import React from "react";
import { Routes, Route } from "react-router-dom";

import ProductPreview from "./page/product/product-preview/product-preview.component";

import Footer from "./components/footer/footer.component";
import ProductAddPage from "./page/product/product-add/product-add.component";

import "./App.css";

function App() {
  return (
    <div>
      <Routes>
        <Route path="/" element={<ProductPreview />} />
        <Route path="/add-product" element={<ProductAddPage />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
