import React from "react";

import Directory from "../../../components/product/directory/directory.component";
import HeaderPreview from "../../../components/product/header/header-preview/header-preview.component";

import "./product-preview.styles.scss";

const ProductPreview = () => {
  const checkboxId = [];

  const checkboxDeleteList = (id) => {
    const result = checkboxId.find((data) => data == id);

    if (!!result) {
      checkboxId.splice(
        checkboxId.findIndex((data) => data === id),
        1
      );
    } else {
      checkboxId.push(id);
    }
  };

  return (
    <div className="product-preview">
      <HeaderPreview idList={checkboxId} />
      <Directory checkboxDeleteList={checkboxDeleteList} />
    </div>
  );
};

export default ProductPreview;
