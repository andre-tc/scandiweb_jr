import React from "react";
import axios from "../../../api/api";

import ProductItem from "../product-item/product-item.component";

import "./directory.styles.scss";

const Directory = ({ checkboxDeleteList }) => {
  const [products, setProducts] = React.useState([]);

  /**
   * Get products from data base and set it in a variable.
   */
  const getProducts = async () => {
    try {
      await axios.get("/").then((res) => {
        const data = res.data;
        setProducts(data);
      });
    } catch (e) {
      console.log("error: ", e);
    }
  };

  React.useEffect(() => {
    getProducts();
  }, []);

  return (
    <div className="product-preview">
      {products.map((productItem) => (
        <ProductItem
          key={productItem.id}
          productItem={productItem}
          checkboxDeleteList={checkboxDeleteList}
        />
      ))}
    </div>
  );
};

export default Directory;
