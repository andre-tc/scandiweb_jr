import React from "react";
import { useNavigate } from "react-router-dom";
import CustomButton from "../../../custom-button/custom-button.component";

import axios from "../../../../api/api";

import "./header-preview.styles.scss";

const HeaderPreview = ({ idList }) => {
  const navigate = useNavigate();

  /**
   * Delete product from database
   * @param {array} checkboxId
   * @return boolean
   */
  const deleteProduct = async (checkboxId) => {
    const idDelete = { id: checkboxId };

    try {
      await axios
        .post("/deleteProduct", JSON.stringify(idDelete))
        .then(() => window.location.reload());
    } catch (error) {
      console.log({ error });
    }

    return true;
  };

  return (
    <div className="header-container">
      <div className="title">Product List</div>
      <div className="buttons-container">
        <CustomButton
          id="add-product-btn"
          onClick={() => navigate("/add-product")}
        >
          ADD
        </CustomButton>
        <CustomButton
          id="delete-product-btn"
          onClick={() => deleteProduct(idList)}
        >
          MASS DELETE
        </CustomButton>
      </div>
    </div>
  );
};

export default HeaderPreview;
