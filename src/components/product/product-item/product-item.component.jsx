import React from "react";
import "./product-item.styles.scss";

const ProductItem = ({
  productItem: { id, sku, name, price, product, size },
  checkboxDeleteList,
}) => {
  const [checkbox, setCheckbox] = React.useState(false);

  const toggleCheckbox = () => {
    setCheckbox(!checkbox);
  };

  /**
   * Return product dimensions based on product value.
   * @param {string} product
   * @returns {string}
   */
  const setLabel = (product) => {
    return product === "Forniture"
      ? "Dimension"
      : product === "Book"
      ? "Weight"
      : "Size";
  };

  return (
    <div className="product-container">
      <div
        className="delete-checkbox"
        onClick={() => {
          toggleCheckbox(id);
          checkboxDeleteList(id);
        }}
      >
        {checkbox ? <span>&#9745;</span> : <span>&#9744;</span>}
      </div>
      <div className="product-items">
        <div className="item">{sku}</div>
        <div className="item">{name}</div>
        <div className="item">{`${price} $`}</div>
        <div className="item">{`${setLabel(product)}: ${size}`}</div>
      </div>
    </div>
  );
};

export default ProductItem;
