const moneyMask = (value) => {
  return value
    .replace(/\D/g, "") // only allow digits
    .replace(/(\d{1})(\d{12})$/, "$1,$2") // put a dot before the last digits
    .replace(/(\d{1})(\d{11})$/, "$1,$2") // put a dot before the last 11 digits
    .replace(/(\d{1})(\d{8})$/, "$1,$2") // put a dot before the last 8 digits
    .replace(/(\d{1})(\d{5})$/, "$1,$2") // put a dot before the last 5 digits
    .replace(/(\d{1})(\d{1,2})$/, "$1.$2"); // put a dot before the last 4 digits
};

const skuMask = (value) => {
  return value.replace(/[^a-zA-Z0-9-]$/g, "");
};

const sizeMask = (value) => {
  return value.replace(/\D/g, "");
};

const allMask = (type, value) => {
  switch (type) {
    case "price":
      return moneyMask(value);
    case "sku":
      return skuMask(value);
    case "height":
    case "width":
    case "length":
    case "size":
      return sizeMask(value);
    default:
      return value;
  }
};

export default allMask;
