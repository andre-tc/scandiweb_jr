import React from "react";

import mask from "../mask";

import "./form-input.styles.scss";

/**
 * Custom input form that can validate onChange with a mask
 * @param {function} handleChange
 * @param {string} label
 * @param {boolean} error
 * @param {string} errorMessage
 * @param {*} otherProps other properties to input
 * @returns
 */

const FormInput = ({
  handleChange,
  label,
  error = false,
  errorMessage = "",
  ...otherProps
}) => (
  <div className="input-container">
    <input
      className={`${!!error ? "error-input" : ""} form-input`}
      onChange={(event) =>
        handleChange(
          event.target.name,
          mask(event.target.name, event.target.value)
        )
      }
      {...otherProps}
    />
    {label && (
      <label
        className={`${
          otherProps.value.length ? "shrink" : ""
        } form-input-label`}
      >
        {label}
      </label>
    )}
    {error && <div className="error-label">{`*${errorMessage}`}</div>}
  </div>
);

export default FormInput;
