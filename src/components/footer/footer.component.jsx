import React from "react";

import "./footer.styles.scss";

const Footer = () => (
  <div className="footer">
    <div className="title">Scandiweb Test Assignment</div>
  </div>
);

export default Footer;
