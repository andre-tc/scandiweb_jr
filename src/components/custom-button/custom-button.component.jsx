import React from "react";

import "./custom-button.styles.scss";

const CustomButton = ({ id, children, ...otherProps }) => (
  <div>
    <button id={id} {...otherProps}>
      {children}
    </button>
  </div>
);

export default CustomButton;
