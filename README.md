**Scandiweb website**

This project is a website to scandiweb junior developer test. 

In this project you will be able to:
	
	1. Create a product.
	2. List all products.
	3. Delete specifics products.
	
There are two pages on this website:

	1. ListProducts.
	2. AddProducts.

---

## Installing necessary packages

Install the package manager of your preference.

[yarn](https://yarnpkg.com/getting-started/) or [npm](https://docs.npmjs.com/about-npm)
	
Run this command on your project's terminal:

	yarn install or npm install

---

## Running the website

Run this command on your project's terminal:

	yarn start or npm start

---

## Accessing the database

The **BASE_URL** to access your database is in:
	
	api_scandiwe_jr > app > Routes > Config.php
  
	<?php

		define("URL_BASE", "define_here_your_database_link");
	?>
  
---

## Connecting to database

The connection to api is inside of file **api.js**

	`scandiweb_jr > src > api.js`
	
	const api = axios.create({
  	baseURL:
   		"write_here_the_api_connection_url",
	});
	
	
--- 

## Ready get started

That's it, now you are ready get started.